# Day 1 of Advent of Code 2019
class Launcher():
    modules:list = None

    def __init__(self, parser=None):
        if parser:
            p = parser.add_parser("launcher")
            p.add_argument('modules', help='Filepath to module masses')
            p.add_argument('-r', '--recursive', help='Get recursive fuel cost (part 2)', action="store_true")


    def run(self, args):
        """
        Runs the launcher based on command-line arguments
        """
        self.load_modules(args.modules)
        print(f"Launcher Fuel Cost: {self.get_fuel_cost(args.recursive)}")


    def load_modules(self, module_list_path:str):
        """
        Takes a filepath to a file that contains module masses
        The fileformat is masses delimited by newlines
        Then stores the list into the launcher
        """
        self.modules = []
        self.fuel_cost = 0

        with open(module_list_path, "r") as modules_list:
            for module in modules_list:
                module_mass = int(module)
                self.modules.append(module_mass)


    def get_fuel_cost(self, recursive:bool=False) -> int:
        """
        Uses the internal modules list to determine how much fuel we will use
        If recursive is true, it will continue calculating fuel cost for a module until it is 0 or negative.

        Return value is the total fuel cost.
        """
        fuel_cost = 0

        def get_cost(mass:int) -> int:
            cost = int(mass/3) - 2
            if cost <= 0:
                return 0
            return cost + get_cost(cost)

        for module in self.modules:
            if recursive:
                fuel_cost += get_cost(module)
            else:
                fuel_cost += int(module/3) - 2

        return fuel_cost

