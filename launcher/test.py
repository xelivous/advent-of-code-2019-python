import unittest, os
from pathlib import Path
from launcher import Launcher

DIR_PATH = Path(os.path.dirname(os.path.abspath(__file__)))

class TestLauncherPart1(unittest.TestCase):
    def setUp(self):
        self.launcher = Launcher()
    
    def tearDown(self):
        self.launcher = None

    def assert_fuel_cost(self, modules:list, cost:int):
        self.launcher.modules = modules
        output = self.launcher.get_fuel_cost(False)
        self.assertEqual(output, cost)

    def test_part1_example1(self):
        self.assert_fuel_cost([12], 2)

    def test_part1_example2(self):
        self.assert_fuel_cost([14], 2)

    def test_part1_example3(self):
        self.assert_fuel_cost([1969], 654)

    def test_part1_example4(self):
        self.assert_fuel_cost([100756], 33583)

    def test_part1_real(self):
        self.launcher.load_modules(DIR_PATH / "test_input")
        output = self.launcher.get_fuel_cost(False)
        self.assertEqual(output, 3392373)

class TestLauncherPart2(unittest.TestCase):
    def setUp(self):
        self.launcher = Launcher()
    
    def tearDown(self):
        self.launcher = None

    def assert_fuel_cost(self, modules:list, cost:int):
        self.launcher.modules = modules
        output = self.launcher.get_fuel_cost(True)
        self.assertEqual(output, cost)
        
    def test_part2_example1(self):
        self.assert_fuel_cost([14], 2)

    def test_part2_example2(self):
        self.assert_fuel_cost([1969], 966)

    def test_part2_example3(self):
        self.assert_fuel_cost([100756], 50346)

    def test_part2_real(self):
        self.launcher.load_modules(DIR_PATH / "test_input")
        output = self.launcher.get_fuel_cost(True)
        self.assertEqual(output, 5085699)