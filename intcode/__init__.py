# Day 2 of Advent of Code 2019
class IntcodeComputer():
    program:list = None
    RAM:list = None

    def __init__(self, parser=None):
        if parser:
            p = parser.add_parser("intcode")
            p.add_argument('program', help='Filepath to intcode program')
            p.add_argument('-b', '--brute-force', type=int, help='Try every combination of noun and verb until desired value (part 2)')


    def run(self, args):
        """
        Runs the intcode computer based on command-line arguments
        """
        self.load_program(args.program)

        if args.brute_force != None:
            noun, verb = self.brute_force(args.brute_force)

            if noun == -1 and verb == -1:
                print(f"Could not find a noun or verb for the desired output")
            else:
                print(f"Correct noun and verb is: {noun}, {verb}")
                print(f"Also known as {noun*100 + verb}")
        else:
            print(f"Return value is {self.run_program()}")


    def brute_force(self, desired_value:int) -> (int,int):
        """
        Tries running the stored program over and over with different noun/verb values
        until the output (address 0) matches the desired_value
        returns noun, value
        if it can't find the correct noun and verb, returns -1, -1
        """
        for noun in range(0,99):
            for verb in range(0,99):
                output = self.run_program(noun, verb)
                if output == desired_value:
                    return noun, verb
        return -1, -1


    def load_program(self, filepath:str):
        """
        Takes a filepath and loads it into the computer
        A proper file is comma-delimited ints
        Only the first line is parsed
        """
        with open(filepath, "r") as program:
            self.program = program.readline().strip().split(sep=",")
            self.program = [int(i) for i in self.program]


    def run_program(self, noun:int=None, verb:int=None) -> int:
        """ 
        RAM is initialized to the exact copy of the stored program
        For part 1, the noun is 12 and verb is 2, so they are the default values
        Return value
        """
        self.RAM = self.program.copy()
        if noun:
            self.RAM[1] = noun
        if verb:
            self.RAM[2] = verb

        pos:int = 0
        while pos <= len(self.RAM):
            opcode = self.RAM[pos]
            if opcode == 1:
                self.opcode_add(self.RAM[pos:pos+4])
                pos += 4
            elif opcode == 2:
                self.opcode_mul(self.RAM[pos:pos+4])
                pos += 4
            elif opcode == 99:
                pos += 1
                break
        
        return self.RAM[0]


    def opcode_add(self, list:list):
        """
        list[0] is a sanity check of 1
        Takes the value at list[1] in RAM, then adds it to the value at list[2] in ram
        Sets list[3] in RAM to the output value
        """
        assert len(list) == 4, "Input list for opcode ADD should have a length of 4"
        assert list[0] == 1, "Input list for Opcode ADD should start with a 1"

        self.RAM[list[3]] = self.RAM[list[1]] + self.RAM[list[2]]


    def opcode_mul(self, list):
        """
        list[0] is a sanity check of 2
        Takes the value at list[1] in RAM, then multiplies it by the value at list[2] in ram
        Sets list[3] in RAM to the output value
        """
        assert len(list) == 4, "Input list for opcode ADD should have a length of 4"
        assert list[0] == 2, "Input list for Opcode MUL should start with a 2"

        self.RAM[list[3]] = self.RAM[list[1]] * self.RAM[list[2]]

