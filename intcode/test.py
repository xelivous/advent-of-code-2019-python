import unittest, os
from pathlib import Path
from intcode import IntcodeComputer

DIR_PATH = Path(os.path.dirname(os.path.abspath(__file__)))

class TestIntcodeComputerExamples(unittest.TestCase):
    def setUp(self):
        self.computer = IntcodeComputer()
    

    def tearDown(self):
        self.computer = None


    def test_example1(self):
        self.computer.load_program(DIR_PATH / "test_example1")
        output = self.computer.run_program()
        self.assertEqual(self.computer.RAM, [2,0,0,0,99], "addition test of 1+1 failed")


    def test_example2(self):
        self.computer.load_program(DIR_PATH / "test_example2")
        output = self.computer.run_program()
        self.assertEqual(self.computer.RAM, [2,3,0,6,99], "multiplication test of 2*3 failed")
    

    def test_example3(self):
        self.computer.load_program(DIR_PATH / "test_example3")
        output = self.computer.run_program()
        self.assertEqual(self.computer.RAM, [2,4,4,5,99,9801], "multiplication test of 99*99 failed")


    def test_example4(self):
        self.computer.load_program(DIR_PATH / "test_example4")
        output = self.computer.run_program()

        self.assertEqual(self.computer.RAM[4], 2, "Overwriting the 99 in op 4 with a 2 seems to have failed")
        self.assertEqual(self.computer.RAM, [30,1,1,4,2,5,6,0,99])
        

    def test_program_part1(self):
        self.computer.load_program(DIR_PATH / "test_input")
        output = self.computer.run_program(12, 2)
        self.assertEqual(output, 5534943)


    def test_program_part2(self):
        self.computer.load_program(DIR_PATH / "test_input")
        noun, verb = self.computer.brute_force(19690720)
        output = noun * 100 + verb
        self.assertEqual(output, 7603)