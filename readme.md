# Advent of Code 2019

A program that should work on every day for [Advent of Code 2019](https://adventofcode.com/2019/).

My main goal was to not use any external libraries that would require pip.

* Run with `python main.py [submodule_name] [submodule_parameters]`
* Unit test with `python -m unittest`
* Get basic help with `python main.py -h`
* Get help for each submodule with `python main.py {submodule_name} -h`

If you use code-oss with the python extension everything should be set up as normal with `launch.json` and `settings.json`.

## Day 1 - Launcher

* Part 1: `python main.py launcher [filepath_to_modules]`
* Part 2: `python main.py launcher [filepath_to_modules] --recursive`

`./launcher/test_input` contains my input file.

## Day 2 - Intcode Computer

* Part 1: `python main.py intcode [filepath_to_program]`
* Part 2: `python main.py intcode [filepath_to_program] --brute-force 19690720`

`./intcode/test_input` contains my input file.

## Day 3 - Wire Calculator

* Part 1: `python main.py wires [filepath_to_wires]`

`./wires/test_input`  contains my input file.