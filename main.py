

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    subparsers = parser.add_subparsers(help="Which submodule to run?", dest="submodule")
    parser.add_argument("-V", "--version", help="show program version", action="store_true")

    from launcher import Launcher
    from intcode import IntcodeComputer
    from wires import WireCalculator

    submodules = {
        "launcher": Launcher(subparsers), # Day 1
        "intcode": IntcodeComputer(subparsers), # Day 2
        "wires": WireCalculator(subparsers) # Day 3
    }

    args = parser.parse_args()
    submodules[args.submodule.lower()].run(args)