# Day 3 of Advent of Code 2019
class WireCalculator():
    wires:list = None

    def __init__(self, parser=None):
        if parser:
            p = parser.add_parser("wires")
            p.add_argument('data', help='Filepath to wire data')

    
    def run(self, args):
        self.load_wires(args.data)
        points = self.get_intersection_points()
        print(f"Intersection points: {points}")
        distance = self.get_shortest_intersection(points)
        print(f"Closest Manhattan Distance: {distance}")


    def load_wires(self, filepath:str):
        """
        Takes a filepath and loads it
        A proper file is comma-delimited instructions in the format of `/[L/D/U/R]\d+/`
        Each line is a new wire
        """
        self.wires = []
        with open(filepath, "r") as wires:
            for wire_text in wires:
                wire_split = wire_text.strip().split(sep=",")
                wire = Wire(wire_split)
                self.wires.append(wire)


    def get_intersection_points(self) -> list:
        """
        Finds all of the intersection points between the stored wires
        Returns a list of tuples of the points
        """

        intersections = []

        for x in range(0, len(self.wires[0].points)-1):
            for y in range(0, len(self.wires[1].points)-1):
                line1 = (self.wires[0].points[x], self.wires[0].points[x+1])
                line2 = (self.wires[1].points[y], self.wires[1].points[y+1])
                point = self.get_intersection_point(line1, line2)
                if point != (None, None):
                    intersections.append(point)

        return intersections

    def get_intersection_point(self, start:tuple, end:tuple) -> tuple:
        """
        Finds the point where two lines intersect, assuming only vertical/horizontal lines
        ( (0,0) , (0,1) ) tuple of tuple

        returns tuple
        """
        #line 1
        x1 = start[0][0]
        y1 = start[0][1]
        x2 = start[1][0]
        y2 = start[1][1]

        #line2
        x3 = end[0][0]
        y3 = end[0][1]
        x4 = end[1][0]
        y4 = end[1][1]

        finalX = None
        finalY = None

        if x1 == x2: #line1 is vertical
            if y1 >= y2:
                if y1 >= y3 and y2 <= y3:
                    finalX = x1
            else:
                if y2 >= y3 and y1 <= y3:
                    finalX = x1

        if y1 == y2: #line1 is horizontal
            if x1 >= x2:
                if x1 >= x3 and x2 <= x3:
                    finalY = y1
            else:
                if x2 >= x3 and x1 <= x3:
                    finalY = y1

        if y3 == y4: #line2 is horizontal
            if x3 >= x4:
                if x3 >= x1 and x4 <= x1:
                    finalY = y3
            else:
                if x4 >= x1 and x3 <= x1:
                    finalY = y3

        if x3 == x4: #line2 is vertical
            if y3 >= y4:
                if y3 >= y1 and y4 <= y1:
                    finalX = x3
            else:
                if y4 >= y1 and y3 <= y1:
                    finalX = x3
        
        # If our return values are at 0,0 (invalid)
        # or if it's not an actual intersection and just parallel
        # return (none, none)
        if (finalX == None or finalY == None) or (finalX == 0 and finalY == 0):
            return (None, None)

        return (finalX, finalY)


    def get_shortest_intersection(self, points) -> int:
        """
        Finds the shortest manhattan distance intersection
        between two points.

        returns an int
        """
        final_distance:int = None

        for point in points:
            distance = self.get_manhattan_distance((0,0), point)
            if not final_distance or distance < final_distance:
                final_distance = distance
        
        return final_distance


    def get_manhattan_distance(self, start:tuple, end:tuple) -> int:
        """
        Calculates the manhattan distance between start and end
        """
        return (abs(end[0]) + abs(end[1])) - (abs(start[0]) + abs(start[1]))


    def does_bounding_intersect(self, start:tuple, end:tuple) -> bool:
        """
        Checks if two lines intersect based on their bounding box
        Useful to quickly return from a function before more lenghty processing is done
        But not needed when we know both wires are only ever at right angles.
        """
        return (
            start[0][0] <= end[1][0]
            and start[0][1] <= end[1][1]
            and start[1][0] >= end[0][0]
            and start[1][1] >= end[0][1]
        )


class Wire():
    points:list = None

    def __init__(self, definition:list):
        current_pos = (0,0)

        self.points = [current_pos] #start at 0,0
        for entry in definition:
            movement = self.instruction_to_tuple(entry)
            current_pos = tuple(sum(x) for x in zip(current_pos, movement))
            self.points.append(current_pos)


    def __repr__(self):
        return f"Wire Points: {len(self.points)}"


    def instruction_to_tuple(self, inst:str):
        """
        R975 = (975,0)
        L54 = (-54,0)
        U20 = (0,20)
        D300 = (0,-300)
        """
        direction = inst[0]
        amt = int(inst[1:])
        if direction == "D":
            return (0, -amt)
        elif direction == "U":
            return (0, amt)
        elif direction == "L":
            return (-amt, 0)
        elif direction == "R":
            return (amt, 0)
        
        return