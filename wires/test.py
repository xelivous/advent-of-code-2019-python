import unittest, os
from pathlib import Path
from wires import WireCalculator

DIR_PATH = Path(os.path.dirname(os.path.abspath(__file__)))

class TestWireExamples(unittest.TestCase):

    def setUp(self):
        self.wires:WireCalculator = WireCalculator()


    def test_point1(self):
        line1 = ((3,5),(3,2))
        line2 = ((2,3),(6,3))
        point = self.wires.get_intersection_point(line1, line2)
        self.assertEqual(point, (3,3))


    def test_point2(self):
        line1 = ((3,5),(8,5))
        line2 = ((6,7),(6,3))
        point = self.wires.get_intersection_point(line1, line2)
        self.assertEqual(point, (6,5))


    def test_pointfail(self):
        line1 = ((0,0),(0,7))
        line2 = ((8,0),(8,5))
        point = self.wires.get_intersection_point(line1, line2)
        self.assertEqual(point, (None, None))
        pass


    def test_example1(self):
        self.wires.load_wires(DIR_PATH / "test_example1")
        points = self.wires.get_intersection_points()
        self.assertEqual(points, [(6,5), (3,3)])
        distance = self.wires.get_shortest_intersection(points)
        self.assertEqual(distance, 6)


    def test_example2(self):
        self.wires.load_wires(DIR_PATH / "test_example2")
        points = self.wires.get_intersection_points()
        distance = self.wires.get_shortest_intersection(points)
        self.assertEqual(distance, 159)


    def test_example3(self):
        self.wires.load_wires(DIR_PATH / "test_example3")
        points = self.wires.get_intersection_points()
        distance = self.wires.get_shortest_intersection(points)
        self.assertEqual(distance, 135)


    def test_personal1(self):
        self.wires.load_wires(DIR_PATH / "test_personal1")
        points = self.wires.get_intersection_points()
        distance = self.wires.get_shortest_intersection(points)
        self.assertEqual(points, [(10,0)])


    def test_realdeal(self):
        self.wires.load_wires(DIR_PATH / "test_input")
        points = self.wires.get_intersection_points()
        distance = self.wires.get_shortest_intersection(points)
        self.assertEqual(distance, 248)